# **1. Microservice**
 - **<a href='http://jpub.tistory.com/851' target='_blank'>(Book)자바 기반 마이크로서비스 이해와 아키텍처 구축</a>** 
 
   source code - https://github.com/architectstory <br>
   <pre>
     github source code project description
         msa-book                           : 마이크로서비스
            msa-service-coffee-order        : 커피주문
            msa-service-coffee-member       : 회워조회 
            msa-service-coffee-status       : 주문처리상태조회
         msa-architecture-config-server     : 환경설정
         msa-architecture-zuul-server       : API Gateway
         msa-architecture-eureka-server     : 마이크로서비스 등록 및 감지
         msa-architecture-turbine-server    : 마이크로서비스 스트림 메시지 수집 
         msa-architecture-hystrix-dashboard : 마이크로서비스 모니터링
   </pre>
                 
    installation guide (video) <br>
   <pre>
        <a href='http://www.hoony.com/hoony/msa-book/msa_kafka_install.mp4'  target='_blank'>1. install kakfa </a>
        2. Install eclipse IDE, sts, gradle plugin, lombok library  working on....
        3. Download ConfigServr, Eureka, Zuul and msa-book working on....
    
   </pre>                

 
---------------------------------------
# **2. Blockchain**
